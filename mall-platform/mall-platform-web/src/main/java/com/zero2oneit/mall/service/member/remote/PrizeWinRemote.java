package com.zero2oneit.mall.service.member.remote;

import com.zero2oneit.mall.common.annotion.OperateLog;
import com.zero2oneit.mall.common.bean.member.PrizeWin;
import com.zero2oneit.mall.common.bean.member.PrizeWinMapping;
import com.zero2oneit.mall.common.enums.BusinessType;
import com.zero2oneit.mall.common.query.member.PrizeWinMappingQueryObject;
import com.zero2oneit.mall.common.query.member.PrizeWinQueryObject;
import com.zero2oneit.mall.common.utils.R;
import com.zero2oneit.mall.common.utils.bootstrap.BoostrapDataGrid;
import com.zero2oneit.mall.feign.member.PrizeFeign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Description:
 *
 * @author Cmx
 * @date 2021/5/25 15:49
 */

@RestController
@RequestMapping("/remote/prizeWin")
@CrossOrigin
public class PrizeWinRemote {

    @Autowired
    private PrizeFeign ruleFeign;

    /**
     * 查询中奖会员列表信息
     * @param qo
     * @return
     */
    @PostMapping("/winList")
    public BoostrapDataGrid winList(@RequestBody PrizeWinQueryObject qo){
        return ruleFeign.winList(qo);
    }

    /**
     * 更改中奖会员信息状态
     * @param id
     * @param status
     * @return
     */
    @PostMapping("/IssueStatus")
    public R IssueStatus(@RequestParam("id") String id, @RequestParam("status") Integer status){
        return ruleFeign.IssueStatus(id, status);
    }

    /**
     * 查询指定中奖会员列表信息
     * @param qo
     * @return
     */
    @PostMapping("/list")
    public BoostrapDataGrid list(@RequestBody PrizeWinMappingQueryObject qo){
        return ruleFeign.list(qo);
    }

    /**
     * 添加或编辑指定中奖会员信息
     * @param prizeWinMapping
     * @return
     */
    @PostMapping("/addOrEdit")
    public R addOrEdit(@RequestBody PrizeWinMapping prizeWinMapping){
        return ruleFeign.addOrEdit(prizeWinMapping);
    }

    /**
     * 删除指定中奖会员信息
     * @param ids
     * @return
     */
    @OperateLog(title = "删除中奖会员记录信息", businessType = BusinessType.DELETE)
    @PostMapping("/delByIds")
    public R delByIds(String ids){
        return ruleFeign.delByIds(ids);
    }

    /**
     * 更改指定中奖会员信息状态
     * @param id
     * @param status
     * @return
     */
    @PostMapping("/status")
    public R status(@RequestParam("id") String id, @RequestParam("status") Integer status){
        return ruleFeign.status(id, status);
    }

}
